/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  David Edmundson <davidedmundson@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SKYPEDATASOURCE_H
#define SKYPEDATASOURCE_H

#include <basepersonsdatasource.h>

class QSqlDatabase;

class SkypeDataSource : public KPeople::BasePersonsDataSource
{
public:
    SkypeDataSource(QObject *parent, const QVariantList &args = QVariantList());
    virtual ~SkypeDataSource();
    virtual KPeople::AllContactsMonitor* createAllContactsMonitor();
    virtual KPeople::ContactMonitor* createContactMonitor(const QString& contactId);
    virtual QString sourcePluginId() const;

private:
    QSqlDatabase *m_db; //Probably there should be list of db - for each account
};

#endif // SKYPEDATASOURCE_H
