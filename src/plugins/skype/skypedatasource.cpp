/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  David Edmundson <davidedmundson@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "skypedatasource.h"

#include <QSqlQuery>
#include <QSqlDatabase>

#include <KABC/Addressee>

#include <KPluginFactory>
#include <KPluginLoader>

// Keep ContactColumns list and enum in coherent state!
static const QStringList contactColumns = QStringList() << "skypename"
                                                        << "birthday"
                                                        << "phone_home"
                                                        << "phone_office"
                                                        << "phone_mobile"
                                                        << "province"
                                                        << "city"
                                                        << "homepage"
                                                        << "avatar_image"
                                                        << "timezone"
                                                        << "availability"
                                                        << "displayname"
                                                        << "given_displayname";

enum ContactColumnsEnum {
    SkypeName,
    Birthday,
    PhoneHome,
    PhoneOffice,
    PhoneMobile,
    Province,
    City,
    Homepage,
    AvatarImage,
    Timezone,
    Availability,
    DisplayName,
    GivenDisplayName
};

static const QString baseQueryText = QLatin1String("SELECT ") + contactColumns.join(",") + QLatin1String(" FROM Contacts");
static const int SkypeUtcTimeConstant = 86400;
static const QLatin1String dbFilename("/home/user/.Skype/skype-account-name/main.db"); // Dev: Adjust this path to get things works

inline KABC::Addressee getContactFromSkypeModel(QSqlQuery *pQuery)
{
    KABC::Addressee skypeContact;
    skypeContact.setFormattedName(pQuery->value(DisplayName).toString());
    skypeContact.setGivenName(pQuery->value(GivenDisplayName).toString());
    skypeContact.setBirthday(QDateTime::fromString(pQuery->value(Birthday).toString(), QLatin1String("yyyyMMdd")));

    QString phoneNumber;

    phoneNumber = pQuery->value(PhoneHome).toString();
    if (!phoneNumber.isEmpty()) {
        skypeContact.insertPhoneNumber(KABC::PhoneNumber(phoneNumber, KABC::PhoneNumber::Home));
    }

    phoneNumber = pQuery->value(PhoneOffice).toString();
    if (!phoneNumber.isEmpty()) {
        skypeContact.insertPhoneNumber(KABC::PhoneNumber(phoneNumber, KABC::PhoneNumber::Work));
    }

    phoneNumber = pQuery->value(PhoneMobile).toString();
    if (!phoneNumber.isEmpty()) {
        skypeContact.insertPhoneNumber(KABC::PhoneNumber(phoneNumber, KABC::PhoneNumber::Cell));
    }

    QByteArray avatarImage = pQuery->value(AvatarImage).toByteArray();

    if (!avatarImage.isEmpty()) {
        skypeContact.setPhoto(KABC::Picture(QImage::fromData(avatarImage.mid(1)))); // Used mid(1) to skip odd extra null
    }

    KABC::Address contactAddress(KABC::Address::Home); // What is proper Address::TypeFlag for that?

    QString portionOfAddressInfo;

    portionOfAddressInfo = pQuery->value(Province).toString();
    if (!portionOfAddressInfo.isEmpty()) {
        contactAddress.setRegion(portionOfAddressInfo);
    }

    portionOfAddressInfo = pQuery->value(City).toString();
    if (!portionOfAddressInfo.isEmpty()) {
        contactAddress.setLocality(portionOfAddressInfo);
    }

    if (!contactAddress.isEmpty())
        skypeContact.insertAddress(contactAddress);

    QString homePage = pQuery->value(Homepage).toString();

    if (!homePage.isEmpty()) {
        skypeContact.setUrl(homePage);
    }

    bool conversionIsOk;
    int skypeTimeZoneValue = pQuery->value(Timezone).toInt(&conversionIsOk);

    if (conversionIsOk) {
        skypeContact.setTimeZone(KABC::TimeZone((skypeTimeZoneValue - SkypeUtcTimeConstant) / 60)); // Skype stores timezone in seconds
    }

    return skypeContact;
}

class SkypeAllContactsMonitor : public KPeople::AllContactsMonitor
{
    Q_OBJECT
public:
    SkypeAllContactsMonitor(QSqlDatabase *db);
    ~SkypeAllContactsMonitor();
    virtual KABC::Addressee::Map contacts();

private:
    KABC::Addressee::Map m_contacts;
};

SkypeAllContactsMonitor::SkypeAllContactsMonitor(QSqlDatabase *db) :
    AllContactsMonitor()
{
    QSqlQuery skypeQuery(*db);
    if (!skypeQuery.exec(baseQueryText + " WHERE " + contactColumns.at(Availability) + " <> 0")) { // Filter out deleted contacts (availability = zero)
        return;
    }


    while (skypeQuery.next()) {
        if (skypeQuery.value(SkypeName).toString() == QLatin1String("echo123")) {
            continue;
        }

        m_contacts["skype://" + skypeQuery.value(SkypeName).toString()] = getContactFromSkypeModel(&skypeQuery);
    }
}

SkypeAllContactsMonitor::~SkypeAllContactsMonitor()
{
}

KABC::Addressee::Map SkypeAllContactsMonitor::contacts()
{
    return m_contacts;
}

class SkypeContactMonitor: public KPeople::ContactMonitor
{
    Q_OBJECT
public:
    SkypeContactMonitor(const QString &contactId, QSqlDatabase *db);
};

SkypeContactMonitor::SkypeContactMonitor(const QString &contactId, QSqlDatabase *db) :
    ContactMonitor(contactId)
{
    QString skypeName = contactId;

    if (skypeName.startsWith("skype://")) {
        skypeName = skypeName.mid(QString("skype://").length());
    }

    QSqlQuery skypeQuery(*db);
    skypeQuery.exec(baseQueryText + QLatin1String(" WHERE ") + contactColumns.at(SkypeName) + " = " + skypeName);

    if (skypeQuery.next()) {
        setContact(getContactFromSkypeModel(&skypeQuery));
    }
}

SkypeDataSource::SkypeDataSource(QObject *parent, const QVariantList &args):
    BasePersonsDataSource(parent)
{
    Q_UNUSED(args);

    m_db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE", QLatin1String("skype-db-connection"))); // There should be account name in connectionName argument (?)
    m_db->setDatabaseName(dbFilename);
    m_db->open();
}

SkypeDataSource::~SkypeDataSource()
{

}

KPeople::AllContactsMonitor *SkypeDataSource::createAllContactsMonitor()
{
    return new SkypeAllContactsMonitor(m_db);
}

KPeople::ContactMonitor *SkypeDataSource::createContactMonitor(const QString &contactId)
{
    return new SkypeContactMonitor(contactId, m_db);
}

QString SkypeDataSource::sourcePluginId() const
{
    return QLatin1String("skype");
}

K_PLUGIN_FACTORY( SkypeDataSourceFactory, registerPlugin<SkypeDataSource>(); )
K_EXPORT_PLUGIN( SkypeDataSourceFactory("skype_kpeople_plugin") )

#include "skypedatasource.moc"
